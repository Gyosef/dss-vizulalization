#!/bin/bash

./processPcaps.sh -c 250 # Split and place pcaps in correct dirs (output/train and output/test)


######################################Create MODELS SEPERATELY

python createTrainCsv.py output/train/ train.csv

python createTestCsv.py output/test/ test.csv train.csv

############## later to be replaced with--- python loadModels.py "train.csv" "test.csv"


python knn.py train.csv test.csv #RUN CUSTOM KNN ON TRAINING AND TEST FILES

python scirun.py "train.csv" "test.csv" #RUN SCIKIT - requires *.csv model created

python createModels.py output/train/ train.csv output/test/ test.csv #CREATE MODELS

python loadModels.py "train.csv" "test.csv" #LOAD MODELS

