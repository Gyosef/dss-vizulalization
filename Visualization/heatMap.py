import plotly 
plotly.tools.set_credentials_file(username='yosefg', api_key='apfpE5kta8mBDR1K3GbH')
import plotly.plotly as py
import plotly.graph_objs as go
import pandas as pd
import numpy as np

df1=pd.read_csv('test.csv')  		##### speciffy which csv file is read

df1= df1.loc[:, (df1 !=0).any(axis=0)]  			#strip out 0's from each column

y= df1['dss-scenario']      						#y axis is dss scenario

rows  = []
count=1

for i in y:											# labeling all to differntiate between same values 
		rows.append(i + "_" + str(count))
		count+= 1
		


X=df1.ix[:, df1.columns != 'instance-number']      # x axis feature sets
XX=X.ix[:, X.columns != 'filename']  

print XX                        				 # XX holds values without filename and instance-number which throws off viz

np_df1= XX.values               				 # convert list type to numpy to have all values in array 


trace = go.Heatmap(z=np_df1,x=XX.columns,y=rows)			#plot


data=[trace]

plotly.offline.plot(data)

