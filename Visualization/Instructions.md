
### What is this repository for? ###

Visulaization for test and train csv files using heatmap

### How do I get set up? ###
Sign up for a plotly account at plot.ly to get access to heatmap and many other visualization API's

Once sign up is complete, edit Line 2 'credentials_file' to match the newly created username and api_key for that specific username. Current setup is to Username-'yosefg' api_key='apfpE5kta8mBDR1K3GbH'

In the heatmap.py, make sure to edit line 8 of the 'read_csv' command to the specific dataset you would like to visualize(test.csv or train.csv). It is currently set to visualize the 'test.csv'

### Comments ###

Currently tested to work with both csv files without any manual editing to fit the heatmap
NOTE-- the filename and instance-number headers are removed automotically for graph fitting purposes 



